#include "Corpus.hpp"

Corpus::Corpus(std::vector<Fichier>& _fichiers,unsigned int _nombre_threads)
:nombre_total_mot(0),nombre_threads(_nombre_threads){
  //parcourir liste _fichiers pour déterminer tous les mot existant
  std::cerr << "debut" << std::endl;
  for(auto i = _fichiers.begin() ; i  != _fichiers.end() ; i++){
    for(auto j = i->nombre_occurence.begin() ; j  != i->nombre_occurence.end() ; j++){
      if(!contient(j->first)){
        idf[j->first] = 1;
      }
    }
  }
  //parcourir tous les mot existant dans le corpus
  unsigned int state = 0;
  for(auto j = idf.begin() ; j  != idf.end() ; j++){
    //on regarde dans chaque fichier s'il contient ce mot, si oui on incrémente sinon on ne fait rien.
    state++;
    std::cerr << "\rParcours des mots existant : " << (int)(((double)state/idf.size())*100) << "%" << std::flush;
    for(auto i = _fichiers.begin() ; i  != _fichiers.end() ; i++){
      if(i->contient(j->first)){
        j->second += 1;
      }
    }
    //ici on a parcourut tous les fichiers, on peut donc mettre la vrai valeur de idf calculer avec le log10.
    j->second = log10( (double)_fichiers.size()/ (j->second - 1) ); // -1 à cause du 1 poser à l'initialisation, un mot a été compter deux fois
  }
  std::cerr << std::endl;

  state = 0;
  //on rajoute maintenant les fichier au corpus mais en changeant le nombre d'occurence de chaque terme par son tf_idf.
  for(auto i = _fichiers.begin() ; i  != _fichiers.end() ; i++){
    state++;
    std::cerr << "\rCalcule tf_idf : " << (int)(((double)state/_fichiers.size())*100) << "%" << std::flush;
    Fichier fichier(*i);
    for(auto j = fichier.nombre_occurence.begin() ; j  != fichier.nombre_occurence.end() ; j++){
      double tf = j->second/fichier.nombre_total_mot;
      j->second = tf * idf[j->first]; // la cle de idf existe necessairement
    }
    fichiers.push_back(fichier);
  }
  std::cerr << std::endl;

  //Tous les tf_idf sont maintenant calculé

  //Elagage du nombre de mot
  std::map<std::string,double> elagage_map;
  std::queue<Fichier*> queue_fichiers;
  for(auto fich = fichiers.begin() ; fich != fichiers.end() ; fich++ ){
    queue_fichiers.push(&(*fich));
  }

  //Récupération des plus grand tf_idf parmi les documents du corpus pour chaque mot;

  state = 0;
  for(auto i = idf.begin() ; i != idf.end() ; i++ ){
    state++;
    std::cerr << "\rChoix des termes pertinents : " << (int)(((double)state/idf.size())*100) << "%" << std::flush;
    for(auto j = fichiers.begin() ; j != fichiers.end() ; j++ ){
      if( j->contient(i->first) && (elagage_map.find(i->first) == elagage_map.end() || elagage_map[i->first] < j->nombre_occurence[i->first]) ){
        elagage_map[i->first] = j->nombre_occurence[i->first];
      }
    }
  }
  std::cerr << std::endl;

  //on garde les mots dont les max extrait ci-dessus sont les plus grand;
  std::vector<std::string> terme_pertinent;
  state = (NB_TERM_GARDE > elagage_map.size())?elagage_map.size():NB_TERM_GARDE;
  for(int i = 0 ; i < NB_TERM_GARDE && !elagage_map.empty() ; i++){
    std::cerr << "\rRécupération des termes pertinents : " << (int)(((double)((NB_TERM_GARDE > elagage_map.size())?elagage_map.size():NB_TERM_GARDE)/state)*100) << "%" << std::flush;
    terme_pertinent.push_back(extraire_max(elagage_map));
  }
  std::cerr << std::endl;

  state = queue_fichiers.size();
  for(unsigned int i = 0 ; i < nombre_threads ; i++){
    threads[i] = std::thread(elage_fichier,&terme_pertinent,&queue_fichiers,state);
  }
  for(unsigned int i = 0 ; i < nombre_threads ; i++){
    if(threads[i].joinable()){
      threads[i].join();
    }
  }
  std::cerr << std::endl;
  //On a tous les termes les plus pertinents, il faut supprimer de notre représentation tous les autres
  //
  //  for(auto f = fichiers.begin() ; f != fichiers.end() ; f++){
  //    to_remove.clear();
  //    //détéction des termes non pertinents
  //    for(auto it = f->nombre_occurence.begin() ; it != f->nombre_occurence.end() ; it++){
  //      if(!trouver_vecteur(it->first,terme_pertinent)){
  //        to_remove.push_back(it->first);
  //      }
  //    }
  //    //suppression des termes non pertinent
  //
  //    for(auto it = to_remove.begin() ; it != to_remove.end() ; it++){
  //      f->nombre_occurence.erase( *it );
  //    }
  //}

  std::vector<std::string> to_remove;
  for(auto it = idf.begin() ; it != idf.end() ; it++){
    if(!trouver_vecteur(it->first,terme_pertinent)){
      to_remove.push_back(it->first);
    }
  }
  for(auto it = to_remove.begin() ; it != to_remove.end() ; it++){
    idf.erase( *it );
  }

  std::cerr << "elagage terminé" << std::endl;
  //Faire de meme dans la liste des terme "idf" ? pas utile mais plus cohérent...

  //fin
}

bool Corpus::contient(std::string key)const{
  auto it = idf.find(key);
  return it != idf.end();
}

const std::vector<Fichier>& Corpus::getFichiers(void)const{
  return fichiers;
}

std::string Corpus::extraire_max(std::map<std::string,double>& _map){
  std::string result;
  double valeur = -1;
  for(auto i = _map.begin() ; i != _map.end() ; i++){
    if(valeur < i->second){
      valeur = i->second;
      result = i->first;
    }
  }
  _map.erase(result);
  return result;
}

bool Corpus::trouver_vecteur(std::string key, std::vector<std::string>& _vect){
  for(auto it = _vect.begin() ; it != _vect.end() ; it++){
    if(it->compare(key) == 0){return true;}
  }
  return false;
}

void Corpus::getListeTerme(std::vector<std::string>& liste)const{
  for(auto it = idf.begin() ; it != idf.end() ; it ++){
    liste.push_back(it->first);
  }
}



void Corpus::elage_fichier(std::vector<std::string>* _liste_termes,std::queue<Fichier*>* _queue_fichiers, unsigned int state){
  if(_liste_termes == NULL ||_queue_fichiers == NULL){
    return;
  }
  bool fin = false;
  while(!fin){
    mtx_elagage.lock();
    if(_queue_fichiers->empty()){
      fin = true;
      mtx_elagage.unlock();
      continue;
    }
    Fichier * f = _queue_fichiers->front();
    _queue_fichiers->pop();
    std::cerr << "\rElagage : " << (int)(((double)state-_queue_fichiers->size())*100/state) << " %" << std::flush;
    mtx_elagage.unlock();
    std::vector<std::string> to_remove;
    //détéction des termes non pertinents
    for(auto it = f->nombre_occurence.begin() ; it != f->nombre_occurence.end() ; it++){
      if(!trouver_vecteur(it->first,*_liste_termes)){
        to_remove.push_back(it->first);
      }
      //suppression des termes non pertinent

      for(auto it = to_remove.begin() ; it != to_remove.end() ; it++){
        f->nombre_occurence.erase( *it );
      }
    }
  }
}
