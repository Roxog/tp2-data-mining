#include "Outils.hpp"

bool outils::verifier_fichier(std::string nom_fichier){
  std::ifstream fichier(nom_fichier);
  if(fichier){
    std::regex pattern("NSF Program[\t ]*:(.*comput.*)$",std::regex::icase | std::regex::ECMAScript);
    std::string ligne;
    while(getline(fichier, ligne))
    {
      std::smatch matche;
      //std::ptrdiff_t nombre_correspondance = std::distance( std::sregex_iterator(ligne.begin(), ligne.end(), pattern), std::sregex_iterator());
      //if(nombre_correspondance != 0){
      if( std::regex_search(ligne,matche,pattern) ){
        nsf_prog[nom_fichier] = matche[1];
        return true;
      }
    }
    fichier.close();
  }
  return false;
}

void outils::copier_fichier(std::string fichier_original, std::string fichier_destination){
  //cout << fichier_original << " -> " << fichier_destination << endl;
  std::ifstream in(fichier_original);
  std::ofstream out(fichier_destination);
  std::locale loc;
  if(in && out){
    //copie simple mot pour mot
    std::regex pattern("Abstract[\t ]*:.*$",std::regex::icase | std::regex::grep);
    std::string ligne;

    while(getline(in, ligne))
    {
      std::ptrdiff_t nombre_correspondance = std::distance( std::sregex_iterator(ligne.begin(), ligne.end(), pattern), std::sregex_iterator());
      if(nombre_correspondance != 0){
        break;
      }
    }
    out << nsf_prog[fichier_original] << std::endl;
    for (std::string s; in >> s; ){
      if(filtre_caractere(s)){
        continue;
      }
      for (std::string::size_type i = 0 ; i < s.length(); i++){
        s[i] = std::tolower(s[i],loc);
      }
      out << s << ' ';
    }
    in.close();
    out.close();
  }
}

void outils::parcourir_dossier(std::string nom_rep, std::string parent_dir, std::string dest_copie){
  std::string chemin = "";
  if(parent_dir.compare("") != 0){
    chemin += parent_dir + "/" + nom_rep;
  }
  else{
    chemin += nom_rep;
  }
  DIR *dir;
  struct dirent *ent;
  std::cout << chemin << std::endl;
  if ((dir = opendir (chemin.c_str())) != NULL) {
    /* print all the files and directories within directory */
    while ((ent = readdir (dir)) != NULL) {
      struct stat stat_fichier;
      std::string nom_fichier = chemin + "/" + ent->d_name;
      stat(nom_fichier.c_str(),&stat_fichier);
      if(S_ISDIR(stat_fichier.st_mode) && strcmp(ent->d_name,".") != 0 && strcmp(ent->d_name,"..") != 0){
        parcourir_dossier(ent->d_name, chemin, dest_copie);
      }
      else if(!S_ISDIR(stat_fichier.st_mode) && verifier_fichier(nom_fichier)){
        copier_fichier(nom_fichier, dest_copie + "/" + ent->d_name);
      }
    }
    closedir (dir);
  }
  else {
    /* could not open directory */
    perror ("opendir");

  }
}

void outils::analyse_repertoire(std::string chemin_repertoire, std::vector<Fichier>& corpus){
  corpus.clear();
  DIR *dir;
  struct dirent *ent;
  if ((dir = opendir (chemin_repertoire.c_str())) != NULL) {
      while ((ent = readdir (dir)) != NULL) {
        struct stat stat_fichier;
        std::string nom_fichier = chemin_repertoire + "/" + ent->d_name;
        stat(nom_fichier.c_str(),&stat_fichier);
        if(S_ISREG(stat_fichier.st_mode)){
          corpus.push_back(Fichier(nom_fichier));
        }
      }
  }
}

bool outils::filtre_caractere(std::string& mot){
  const std::string caractere_interdit = CARACTERE_INTERDIT;
  std::locale loc;
  for (std::string::size_type i = 0 ; i < mot.length(); i++){
    bool garder_caractere = true;
    for (std::string::size_type j = 0 ; j < caractere_interdit.length(); j++){
      if(mot[i] == caractere_interdit[j] ){
        garder_caractere = false;
        break;
      }
    }

    if(garder_caractere){
      mot[i] = std::tolower(mot[i],loc);
    }
    else{
      mot.erase(i,1);
      i--;
    }
  }
  return (mot.compare("") == 0);
}

void outils::get_fichiers(std::string repertoire , std::vector<std::string>& liste_fichiers){
  DIR *dir;
  struct dirent *ent;

  liste_fichiers.clear();
  if(repertoire[repertoire.length()-1] == '/'){
    repertoire.erase(repertoire.length()-1,1);
  }
  //std::cout << chemin << std::endl;
  if ((dir = opendir (repertoire.c_str())) != NULL) {
    while ((ent = readdir (dir)) != NULL) {
      struct stat stat_fichier;
      std::string nom_fichier = repertoire + "/" + ent->d_name;
      stat(nom_fichier.c_str(),&stat_fichier);
      if( !S_ISDIR(stat_fichier.st_mode) && verifier_fichier(nom_fichier) ){
        liste_fichiers.push_back(nom_fichier);
      }
    }
    closedir (dir);
  }
  else {
    /* could not open directory */
    perror ("opendir");
  }
}

void outils::get_repertoire(std::string repertoire , std::vector<std::string>& liste_repertoires){
  DIR *dir;
  struct dirent *ent;

  liste_repertoires.clear();
  if(repertoire[repertoire.length()-1] == '/'){
    repertoire.erase(repertoire.length()-1,1);
  }
  //std::cout << chemin << std::endl;
  if ((dir = opendir (repertoire.c_str())) != NULL) {
    while ((ent = readdir (dir)) != NULL) {
      struct stat stat_fichier;
      std::string nom_repertoire = repertoire + "/" + ent->d_name;
      stat(nom_repertoire.c_str(),&stat_fichier);
      if( S_ISDIR(stat_fichier.st_mode) && strcmp(ent->d_name,".") != 0 && strcmp(ent->d_name,"..") != 0 ){
        liste_repertoires.push_back(nom_repertoire);
      }
    }
    closedir (dir);
  }
  else {
    /* could not open directory */
    perror ("opendir");
  }
}

void outils::get_liste_repertoire(std::string repertoire, std::queue<std::string>& liste_repertoires){
  /*
    Le but de cette fonction est de récuperer tous les répertoire à explorer.
  */
  //On vide le containeur d'accueil par précaution
  while(!liste_repertoires.empty()){liste_repertoires.pop();}

  std::queue<std::string> explorer;
  explorer.push(repertoire);

  while(!explorer.empty()){
    std::vector<std::string> temp;
    get_repertoire(explorer.front(),temp);
    explorer.pop();
    for(auto it = temp.begin() ; it != temp.end() ; it++){
      liste_repertoires.push(*it);
      explorer.push(*it);
    }
  }
}

void outils::developper_list_fichier_si_besoin(std::queue<std::string>& liste_repertoires, std::queue<std::string>& liste_fichiers){
  /*
  Le but de cette fonction est de ne pas avoir la liste complete des fichiers en mémoire d'un coup mais de la découvrir répertoire par répertoire.
  */
  while(!liste_repertoires.empty() && liste_fichiers.empty() ){
    //Ici la liste des répertoire n'est pas vide et il n'y a plus de fichiers à consulter.
    //On continue d'explorer des répertoire tant qu'on a pas de fichiers à traiter et qu'il reste des répertoire à explorer.
    std::vector<std::string> temp;
    std::cout<< "(" << count << ") "<< liste_repertoires.front() << std::endl;
    get_fichiers(liste_repertoires.front(),temp);
    for(auto it = temp.begin() ; it != temp.end() ; it++){
      liste_fichiers.push(*it);
    }
    liste_repertoires.pop();
  }
}

void outils::explore_fichiers(std::string repertoire_destination){
  //On developpe la liste des fichiers si besoin pour l'initialisation;
  mtx.lock();
  developper_list_fichier_si_besoin(rep,fich);
  mtx.unlock();

  while(!fich.empty()){
    mtx.lock();
    //Récupération du nom du fichier
    std::string chemin_fichier = fich.front();
    fich.pop();
    count++;
    developper_list_fichier_si_besoin(rep,fich);
    mtx.unlock();
    //Traitement du fichier :
    //Récupération du nom du fichier pour construire le chemin du fichier de sortie
    std::size_t found = chemin_fichier.find_last_of("/\\");
    std::string nom_fichier = chemin_fichier.substr(found+1);
    copier_fichier(chemin_fichier, repertoire_destination + "/" + nom_fichier);
  }
}
