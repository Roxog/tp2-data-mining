#include "Fichier.hpp"

Fichier::Fichier(){}
Fichier::Fichier(std::string _chemin_fichier)
: nombre_total_mot(0),chemin_fichier(_chemin_fichier)
{
  std::ifstream in(chemin_fichier);
  std::locale loc;
  if(in){
    std::string ligne;
    getline(in, ligne);
    nsf = ligne;
    for (std::string s; in >> s; ){
      //incrémenter correctement la map
      for (std::string::size_type i = 0 ; i < s.length(); i++){
        s[i] = std::tolower(s[i],loc);
      }
      std::map<std::string,double>::iterator it = nombre_occurence.find(s);
      if(it != nombre_occurence.end()){
        nombre_occurence[s] += 1;
      }else{
        nombre_occurence[s] = 1;
      }
      nombre_total_mot++;
    }
    in.close();
  }else{
    throw "Impossible d'ouvrir ce fichier : "+chemin_fichier+"\n";
  }
}

std::string Fichier::to_string() const{
  std::string result = "";
  result += chemin_fichier + " :\n";
  result += "NSF Program : " + nsf + " :\n";
  for(auto it = nombre_occurence.begin() ; it!=nombre_occurence.end() ; it++){
    result += "\t" + it->first + " : " + std::to_string(it->second/nombre_total_mot) + " " + "("+std::to_string(it->second)+"/"+std::to_string(nombre_total_mot)+")" + "\n";
  }
  return result;
}

bool Fichier::contient(std::string key)const{
  auto it = nombre_occurence.find(key);
  return it != nombre_occurence.end();
}

const std::string& Fichier::getChemin(void)const{
  return chemin_fichier;
}

std::ostream& operator<<(std::ostream &strm,const Fichier &fichier){
  return strm << fichier.to_string();
}

inline bool operator==(const Fichier& f1, const Fichier& f2){
  return f1.getChemin().compare(f2.getChemin()) == 0;
}

inline bool operator!=(const Fichier& f1, const Fichier& f2){
  return f1.getChemin().compare(f2.getChemin()) != 0;
}
