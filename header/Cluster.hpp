#ifndef CLUSTER_H
#define CLUSTER_H

#include <vector>
#include <mutex>
#include <map>
#include <ostream>
#include "Fichier.hpp"

class Kmeans;


class Cluster {
  friend class Kmeans;
  friend std::ostream& operator<<(std::ostream &strm,const Cluster &cluster);

  //Definition d'un cluster
  std::vector<const Fichier*> membres;
  std::map<std::string, double> centre;
  std::map<std::string, double> vieux_centre;
  std::mutex mtx;
  Cluster();
  void add(const Fichier* nouveau_membre);
  void calcule_centre();
  void clear();
public:
  Cluster(const Fichier* init_point);
};
std::ostream& operator<<(std::ostream &strm,const Cluster &cluster);

#endif
