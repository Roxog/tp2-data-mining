#ifndef CORPUS_H
#define CORPUS_H

#include "constants.hpp"

#include <vector>
#include <thread>
#include <mutex>
#include <cmath>
#include <map>
#include <queue>
#include "Fichier.hpp"

extern std::mutex mtx_elagage;

class Corpus {
  unsigned int nombre_total_mot;
  unsigned int nombre_threads;
  std::thread threads[MAX_THREAD];
  std::vector<Fichier> fichiers;
  std::map<std::string,double> idf;
  bool contient(std::string key)const;
  static std::string extraire_max(std::map<std::string,double>& _map);
  static void elage_fichier(std::vector<std::string>* _liste_termes,std::queue<Fichier*>* _queue_fichiers, unsigned int state);
  static bool trouver_vecteur(std::string key, std::vector<std::string>& _vect);
public:
  Corpus(std::vector<Fichier>& _fichiers, unsigned int _nombre_threads);
  std::string to_string()const;
  const std::vector<Fichier>& getFichiers(void)const;
  void getListeTerme(std::vector<std::string>& liste)const;
};


#endif
