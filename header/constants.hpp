#ifndef CONSTANT_HPP
#define CONSTANT_HPP

//GENERAL
#define MAX_THREAD 100

//KMEAN
#define NOMBRE_THREAD_DEFAUT 4
#define NOMBRE_ITERATION_DEFAUT 10
#define EPSILON 1e-2

//TRAITEMENT DONNE BRUTE
#define NB_TERM_GARDE 3000

//MESSAGE ARG ANALYSE
#define MANQUE_INFO "Impossible d'analyser les arguments fourni, il manque une information pour l'option : "
#define NOMBRE_TRHEAD_NON_VALIDE_MESSAGE(variable) "Nombre de thread invalide : " << variable << " n'est pas valide."
#define ARGUMENT_NON_VALIDE_MESSAGE " n'est pas une option valide."
#define EMPLACEMENT_DONNEE_MANQUANT_MESSAGE "Vous n'avez pas indique où se trouve les données.\n"
#define DESTINATION_DONNEE_PRETRAITEMENT_MANQUANT_MESSAGE "Vous n'avez pas indique où les données traitées doivent être créées.\n"
#define AUCUN_NOMBRE_THREAD_MESSAGE(variable) "Vous n'avez pas précise le nombre de thread à utilisé. Par défaut " + std::to_string(variable) + " thread seront utilisés.\n"
#define ITERATION_INSUFFISANTE_MESSAGE "Il faut au moins 1 iteration."
#define ITERATION_INFO_MESSAGE "Vous n'avez pas defini le nombre d'iteration à faire.\n Nombre d'iteration par defaut : "

#endif
