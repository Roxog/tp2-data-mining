#ifndef KMEANS_H
#define KMEANS_H

#include <vector>
#include <string>
#include <map>
#include <thread>
#include <mutex>
#include <queue>
#include <ostream>
#include <iomanip>
#include <cmath>
#include <time.h>
#include <stdlib.h>

#include "constants.hpp"

#include "Fichier.hpp"
#include "Corpus.hpp"
#include "Cluster.hpp"

/*
Note pour le multithreading : les calcule de distance se font hors
syncronisation met les controles de distance entre les clusters et
la mise à jour des clusters se fait en synchro sinon pb
*/
class Kmeans {
  friend class Cluster;
  static double distance_euclidienne(const Cluster* cluster, std::vector<std::string>& liste_termes);
  static double distance_euclidienne(const Fichier* point, const Cluster* cluster, std::vector<std::string>* liste_termes);
  static double distance_euclidienne(const Cluster* cluster1, const Cluster* cluster2, std::vector<std::string>& liste_termes);
  double distance_euclidienne(const Fichier* point1, const Fichier* point2);

  const unsigned int nombre_thread;

  Cluster *liste_clusters;
  std::queue<const Fichier*> queue_point_restant;
  std::mutex mtx;

  std::vector<std::string> liste_termes;
  std::vector<const Fichier*> liste_donnees;
  void init_clustering_iteration();
  void init_next_iteration();
  static void attribuer_cluster(std::queue<const Fichier*>* data, Cluster *clusters, unsigned int nombre_cluster, std::vector<std::string>* liste_termes, std::mutex* mtx, unsigned int state);
  bool verifier_stabilite_iteration(Cluster* actuel, Cluster* nouveau,  double _seuil_stabilite = EPSILON);

public:
  const unsigned int nombre_cluster;
  void clusteriser(unsigned int nombre_iteration_max = 1, double _seuil_stabilite = EPSILON);
  void print_result(std::ostream& output);
  Kmeans(Corpus const& corpus, int _nombre_cluster, int _nombre_thread);
  ~Kmeans();
};

int convert_pourcentage(unsigned int val,unsigned int max);

#endif
