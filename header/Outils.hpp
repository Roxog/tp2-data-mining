#ifndef FILTER_H
#define FILTER_H

#include <dirent.h>
#include <string.h>
#include <errno.h>

#include <iostream>
#include <fstream>
#include <regex>

#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>


#include <map>
#include <string>
#include <mutex>
#include <thread>
#include <locale>
#include <vector>
#include <queue>
#include <cstddef> //std::size_t
#include "Fichier.hpp"

#define CARACTERE_INTERDIT ",?;.:/![](){}<>\"'-_"

extern std::mutex mtx;
extern std::queue<std::string> rep;
extern std::queue<std::string> fich;
extern std::map<std::string,std::string> nsf_prog;
extern unsigned int count;

namespace outils{
  bool verifier_fichier(std::string nom_fichier);

  /* deprécié */
  void parcourir_dossier(std::string nom_rep, std::string parent_dir, std::string dest_copie);

  void analyse_repertoire(std::string chemin_repertoire, std::vector<Fichier>& corpus);

  void get_fichiers(std::string repertoire, std::vector<std::string>& liste_fichiers);
  void get_repertoire(std::string repertoire, std::vector<std::string>& liste_fichiers);
  void get_liste_repertoire(std::string repertoire, std::queue<std::string>& liste_repertoires);
  void developper_list_fichier_si_besoin(std::queue<std::string>& liste_repertoires, std::queue<std::string>& liste_fichiers);
  void explore_fichiers(std::string repertoire_destination);

  void copier_fichier(std::string fichier_original, std::string fichier_destination);
  bool filtre_caractere(std::string& mot);
}

#endif
