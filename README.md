# Makefile
Le fichier *Makefile* ne doit pas etre modifie, il gère dynamiquement l'ajout de fichier header ou source.  
Le makefile gère toutes les inclusions, dans les directives du preprocesseur on indique uniquement le nom du fichier. Il ne doit pas etre precede par *header/*.  
## Compilation
Par defaut les options d'optimisation sont active, il suffit de tapez la command *"make"* pour compiler.  
Afin d'activer le debugage avec gdb il faut utiliser la command *"make DEBUG=yes"*, les options d'optimisations seront cependant alors desactivees.
## Nettoyage
*"make clean"* supprime tous les .o generee  
*"make clean_exec"* supprime l'executable  
*"make clean_autolink"* supprime tous les .d generee (fichier contenant les regles d'inclusion des fichiers permettant de generer les .o)  
# Architecture
## Le dossier **source**
Ce dossier contient tous les fichiers sources.  
Ces fichiers se termine par *".cpp"*  
## Le dossier **header**
Ce dossier contient tous les fichiers header.  
Ces fichiers se termine par *".hpp"*  
## Le dossier **bin**
Ce dossier contient uniquement les fichiers intermediaire de compilation.  
